package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/julienschmidt/httprouter"
	"goDbManager/thd_log"
	"goDbManager/thd_misc"
	"goDbManager/thd_postgres"
	"io/ioutil"
	"net/http"
	"os"
	"os/signal"
	"time"
)

type JsonResponse struct {
	Type    string   `json:"type"`
	Data    []string `json:"data"`
	Message string   `json:"message"`
}

type JsonResponseLogin struct {
	ApplicationToken string `json:"access_token"`
	InvalidationTime string `json:"invalidation_time"`
}

type JsonRequestCommon struct {
	Id   string `json:"id"`
	Data string `json:"data"`
}

type JsonResponseCommon struct {
	ApplicationId string `json:"appId"`
	TableName     string `json:"tableName""`
	Data          string `json:"data"`
	Success       bool   `json:"success"`
}

const (
	valueEmpty  string = "value empty"
	doesntExist string = "doesnt exist"
	noIdForSelect string = "no id specified for content selection"
	noDataForInsert string = "no data specified to insert"
)

const phDbId = "dbId"
const phTableName = "tableName"

const urlPathApp = "/app/:" + phDbId
const urlPathAppTable = "/app/:" + phDbId + "/table/:" + phTableName

func writeJSONToHttp(w http.ResponseWriter, response interface{}) {
	js, err := json.Marshal(response)
	if err != nil {
		thd_log.PrintError(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	_, err = w.Write(js)

	thd_log.CheckErr(err)
}

func writeEmptyCommonResponseToHttp(w http.ResponseWriter) {
	var dataArray string
	writeJSONToHttp(w, JsonResponseCommon{ApplicationId: "", TableName: "", Data: dataArray, Success: true})
}

func testRest(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	dbName := "MyTestRest"

	dbCreated := thd_postgres.SetupDB(dbName)

	var response = JsonResponse{Type: "success", Message: fmt.Sprint("DB creation <", dbName, "> success <", dbCreated, ">")}

	writeJSONToHttp(w, response)
}

func isAppAuthorised(appid string, password string) bool {
	return false
	//pass, ok := users[username]
	//if !ok {
	//	return false
	//}
	//
	//return password == pass
}

func appLogin(w http.ResponseWriter, r *http.Request) {
	appId, password, ok := r.BasicAuth()
	if !ok {
		w.Header().Add("WWW-Authenticate", `Basic realm="Give username and password"`)
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte(`{"message": "No basic auth present"}`))
		return
	}

	if !isAppAuthorised(appId, password) {
		w.Header().Add("WWW-Authenticate", `Basic realm="Give username and password"`)
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte(`{"message": "Invalid username or password"}`))
		return
	}

	w.WriteHeader(http.StatusOK)
	writeJSONToHttp(w, JsonResponseLogin{ApplicationToken: ""})

	return
}

func handleAppRequestGET(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	dbId := ps.ByName(phDbId)

	if len(dbId) <= 0 {
		var response = JsonResponseCommon{ApplicationId: dbId, TableName: "", Data: valueEmpty, Success: false}
		writeJSONToHttp(w, response)
		return
	}

	var dataArray string
	var response = JsonResponseCommon{ApplicationId: dbId, TableName: "", Data: dataArray, Success: true}

	writeJSONToHttp(w, response)
}

func handleAppRequestPOST(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	requestData := JsonRequestCommon{}
	readJSONFromRequest(r, &requestData)

	dbId := ps.ByName(phDbId)

	if len(dbId) <= 0 {
		dbId = thd_misc.GenerateUUID()
	}

	initDbSuccess := MaintainAppDb(dbId)
	if initDbSuccess == false {
		thd_log.PrintError("App DB <", dbId, "> not usable")
	}

	var dataArray string
	var response = JsonResponseCommon{ApplicationId: dbId, TableName: "", Data: dataArray, Success: true}

	writeJSONToHttp(w, response)
}

func handleAppTableRequestGET(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	dbId := ps.ByName(phDbId)

	if len(dbId) <= 0 {
		var response = JsonResponseCommon{ApplicationId: dbId, TableName: "", Data: valueEmpty, Success: false}
		writeJSONToHttp(w, response)
		return
	}

	tableName := ps.ByName(phTableName)

	if len(tableName) <= 0 {
		writeJSONToHttp(w, JsonResponseCommon{ApplicationId: dbId, TableName: tableName, Data: valueEmpty, Success: false})
		return
	}

	successWithTable := CheckExistsAppDbTable(dbId, tableName)

	if false == successWithTable {
		writeJSONToHttp(w, JsonResponseCommon{ApplicationId: dbId, TableName: tableName, Data: doesntExist, Success: false})
		return
	}

	requestData := JsonRequestCommon{}
	readJSONFromRequest(r, &requestData)

	if len(requestData.Id) <= 0  {
		writeJSONToHttp(w, JsonResponseCommon{ApplicationId: dbId, TableName: tableName, Data: noIdForSelect, Success: false})
		return
	}

	tDefDefault := AppDefaultTable{}
	tDefDefault.DataId = requestData.Id
	tDefDefault.Data = requestData.Data

	rows := SelectFromTable(dbId, tableName, tDefDefault)

	jsonData, err := json.Marshal(rows)

	thd_log.CheckErr(err)

	dataStr := fmt.Sprintf("%s", jsonData)

	writeJSONToHttp(w, JsonResponseCommon{ApplicationId: dbId, TableName: tableName, Data: dataStr, Success: true})
}

func handleAppTableRequestPOST(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	dbId := ps.ByName(phDbId)

	if len(dbId) <= 0 {
		writeJSONToHttp(w, JsonResponseCommon{ApplicationId: dbId, TableName: "", Data: valueEmpty, Success: false})
		return
	}

	tableName := ps.ByName(phTableName)

	if len(tableName) <= 0 {
		writeJSONToHttp(w, JsonResponseCommon{ApplicationId: dbId, TableName: tableName, Data: valueEmpty, Success: false})
		return
	}

	successWithTable := MaintainAppDbTable(dbId, tableName)

	if false == successWithTable {
		writeJSONToHttp(w, JsonResponseCommon{ApplicationId: dbId, TableName: tableName, Data: doesntExist, Success: false})
		return
	}

	requestData := JsonRequestCommon{}
	readJSONFromRequest(r, &requestData)

	if len(requestData.Data) <= 0 {
		writeJSONToHttp(w, JsonResponseCommon{ApplicationId: dbId, TableName: tableName, Data: noDataForInsert, Success: false})
		return
	}

	if len(requestData.Id) <= 0 {
		requestData.Id = thd_misc.GenerateUUID()
	}

	tDefDefault := AppDefaultTable{}
	tDefDefault.DataId = requestData.Id
	tDefDefault.Data = requestData.Data

	successUpsert := UpsertAppDbTable(dbId, tableName, tDefDefault)

	b, err := json.Marshal(tDefDefault)

	thd_log.CheckErr(err)

	writeJSONToHttp(w, JsonResponseCommon{ApplicationId: dbId, TableName: tableName, Data: string(b), Success: successUpsert})
}

func readJSONFromRequest(r *http.Request, requestObj interface{}) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		thd_log.PrintError(err)
	}
	thd_log.PrintDebug(string(body))
	err = json.Unmarshal(body, requestObj)
	if err != nil {
		thd_log.PrintError(err)
	}
}

func ping(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	w.Header().Set("Server", "A Go Web Server")
	w.WriteHeader(200)
	_, err := fmt.Fprintf(w, "Hello from go_db_manager")
	thd_log.CheckErr(err)
}

func main() {
	counter := 0
	for {
		counter++
		thd_log.PrintInfo("Try to ping DB before startup: ", fmt.Sprintf("%d", counter))
		dbPingSuccess := thd_postgres.PingDB()
		if dbPingSuccess {
			break
		}
	}

	initDbSuccess := MaintainInternalDb()
	if initDbSuccess == false {
		thd_log.PrintError("Internal DB not usable")
		os.Exit(thd_misc.InternalDbErr)
	}

	restPort := os.Getenv(thd_misc.WebservicePort)

	var wait time.Duration
	flag.DurationVar(&wait, "graceful-timeout", time.Second*15, "the duration for which the server gracefully wait for existing connections to finish - e.g. 15s or 1m")
	flag.Parse()

	httpHandler := httprouter.New()
	httpHandler.GET("/ping", ping)
	httpHandler.GET("/test", testRest)

	httpHandler.GET(urlPathApp, handleAppRequestGET)
	httpHandler.POST(urlPathApp, handleAppRequestPOST)

	httpHandler.GET(urlPathAppTable, handleAppTableRequestGET)
	httpHandler.POST(urlPathAppTable, handleAppTableRequestPOST)

	srv := &http.Server{
		Addr: "0.0.0.0:" + restPort,
		// Good practice to set timeouts to avoid Slowloris attacks.
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      httpHandler,
	}

	// Run our server in a goroutine so that it doesn't block.
	go func() {
		thd_log.PrintInfo("Server at: " + restPort)
		if err := srv.ListenAndServe(); err != nil {
			thd_log.PrintError(err)
		}
	}()

	c := make(chan os.Signal, 1)
	// We'll accept graceful shutdowns when quit via SIGINT (Ctrl+C)
	// SIGKILL, SIGQUIT or SIGTERM (Ctrl+/) will not be caught.
	signal.Notify(c, os.Interrupt)

	// Block until we receive our signal.
	<-c

	// Create a deadline to wait for.
	ctx, cancel := context.WithTimeout(context.Background(), wait)
	defer cancel()
	// Doesn't block if no connections, but will otherwise wait
	// until the timeout deadline.
	err := srv.Shutdown(ctx)
	thd_log.CheckErr(err)
	// Optionally, you could run srv.Shutdown in a goroutine and block on
	// <-ctx.Done() if your application should wait for other services
	// to finalize based on context cancellation.
	thd_log.PrintInfo("shutting down")
	os.Exit(0)
}
