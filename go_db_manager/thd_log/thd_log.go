package thd_log

import (
	"fmt"
	"time"
)

// CheckErr Function for handling errors
func CheckErr(err error) {
	if err != nil {
		panic(err)
	}
}

func printLog(level string, message interface{})  {
	t := time.Now()
	fmt.Println(t.Format("[2006-01-02 15:04:05 -07]"), "[", level, fmt.Sprintf("] %s", message))
}

// PrintDebug PrintMessage Function for handling messages
func PrintDebug(message... interface{}) {
	printLog("DEBUG", message)
}

func PrintWarn(message... interface{}) {
	printLog("WARN ", message)
}

func PrintInfo(message... interface{}) {
	printLog("INFO ", message)
}

func PrintError(message... interface{}) {
	printLog("ERROR", message)
}
