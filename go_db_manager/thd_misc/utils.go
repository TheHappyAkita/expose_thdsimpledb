package thd_misc

import (
	"github.com/lithammer/shortuuid"
)

func GenerateUUID() string {
	return shortuuid.New()
}
