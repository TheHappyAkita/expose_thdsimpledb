FROM golang:1.17

ARG WEBSERVICE_PORT

WORKDIR /go/src/app

COPY ./go.mod ./
COPY ./go.sum ./
RUN go mod download

COPY . ./

RUN go build -o /app

EXPOSE $WEBSERVICE_PORT

CMD [ "/app" ]
