FROM alpine:latest

ARG WEBSERVICE_PORT

WORKDIR /app

COPY ./build/linux-amd64/ ./

EXPOSE $WEBSERVICE_PORT

CMD [ "sh", "-c", "/app/goDbManager" ]
