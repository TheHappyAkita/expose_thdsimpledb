FROM golang:1.17 AS build
WORKDIR /src

#need to be set to not dynamically link to os.
#if not activated generated elf file will not run in other containers
ENV CGO_ENABLED=0

COPY ./go.mod ./
COPY ./go.sum ./
RUN go mod download
COPY . ./

ARG TARGETOS=windows
ARG TARGETARCH=amd64
RUN GOOS=${TARGETOS} GOARCH=${TARGETARCH} go build -o /build/${TARGETOS}-${TARGETARCH}/ .

ARG TARGETOS=linux
ARG TARGETARCH=amd64
RUN GOOS=${TARGETOS} GOARCH=${TARGETARCH} go build -o /build/${TARGETOS}-${TARGETARCH}/ .

#necessary to copy compiled file sback to host
#only will be done when container starts
CMD ["sh", "-c", "cp -R /build/. /mnt/build/"]
