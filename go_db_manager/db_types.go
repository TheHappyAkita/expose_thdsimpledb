package main

type DefApplications struct {
	Name     string
	Id       string `db:"id"`
	AppId    string `db:"appId"`
	AppDescr string `db:"appDescr"`
}

type DefAppDefaultTable struct {
	Name string
	Id   string `db:"id"`
	DataId string `db:"dataId""`
	Data string `db:"data"`
}

type Applications struct {
	Id       int    `db:"id"`
	AppId    string `db:"appId"`
	AppDescr string `db:"appDescr"`
}

type AppDefaultTable struct {
	Id   int    `db:"id"`
	DataId string `db:"dataId"`
	Data string `db:"data"`
}

//func GetTableDefApplications() DefApplications {
//	return DefApplications{
//		Name:     "applications",
//		Id:       "serial constraint applications_pk primary key",
//		AppId:    "VARCHAR (50) NOT NULL",
//		AppDescr: "text",
//	}
//}

func GetTableDefApplications() DefAppDefaultTable {
	return GetTableDefAppDefaultTable("applications")
}

func GetTableDefAppDefaultTable(tableName string) DefAppDefaultTable {
	return DefAppDefaultTable{
		Name: tableName,
		Id:   "serial",
		DataId: "varchar constraint " + tableName + "_pk primary key",
		Data: "varchar",
	}
}

type Def_AppTokens struct {
	Name     string
	Id       string `db:"id"`
	AppId    string `db:"appId"`
	AppDescr string `db:"appDescr"`
}

type AppToken struct {
	Id       int    `db:"id"`
	AppId    string `db:"appId"`
	AppDescr string `db:"appDescr"`
}
