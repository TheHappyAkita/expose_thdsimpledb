package thd_postgres

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq" //postgres db driver import, is required for database/sql
	"goDbManager/thd_log"
	"goDbManager/thd_misc"
	"os"
	"strings"
)

// CreateConnectionDBAdmin DB connection set up
func CreateConnectionDBAdmin() (*sql.DB, error) {
	return CreateConnectionDB("")
}

// CreateConnectionDB DB connection set up
func CreateConnectionDB(dbName string) (*sql.DB, error) {
	ldbName := strings.ToLower(dbName)

	dbType := os.Getenv(thd_misc.DbType)
	dbHost := os.Getenv(thd_misc.DbHost)
	dbPort := os.Getenv(thd_misc.DbPort)
	dbUser := os.Getenv(thd_misc.DbUser)
	dbPass := os.Getenv(thd_misc.DbPassword)
	dbSSLMode := "disable"

	dbinfo := ""
	if len(ldbName) <= 0 {
		dbinfo = fmt.Sprintf("host=%s port=%s user=%s password=%s sslmode=%s", dbHost, dbPort, dbUser, dbPass, dbSSLMode)
	} else {
		dbinfo = fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s", dbHost, dbPort, dbUser, dbPass, ldbName, dbSSLMode)
	}

	db, err := sql.Open(dbType, dbinfo)

	thd_log.CheckErr(err)

	return db, err
}

func PingDB() bool  {
	db, err := CreateConnectionDBAdmin()

	if err != nil {
		return false
	}
	defer db.Close()

	err = db.Ping()

	if err != nil {
		return false
	} else {
		return true
	}
}

func CheckDatabaseExists(dbName string) bool {
	thd_log.PrintDebug("Checking Database <", dbName, "> exists...")

	db, err := CreateConnectionDBAdmin()

	chkExists := fmt.Sprintf("select exists( SELECT datname FROM pg_catalog.pg_database WHERE lower(datname) = lower('%s'))", dbName)
	rows, err := db.Query(chkExists)

	thd_log.CheckErr(err)

	for rows.Next() {
		var exists bool
		err = rows.Scan(&exists)

		thd_log.CheckErr(err)

		return exists
	}
	defer rows.Close()

	// check errors
	thd_log.CheckErr(err)

	return false
}

func CreateDatabase(dbName string) bool {
	thd_log.PrintDebug("Database <", dbName, "> will be created...")

	db, err := CreateConnectionDBAdmin()

	_, err = db.Exec("create database " + dbName + ";")

	thd_log.CheckErr(err)

	return true
}

func CheckSchemaExists(dbName string, schemaName string) bool {
	thd_log.PrintDebug("Checking Schema <", schemaName, "> exists in Database <", dbName, ">...")

	db, err := CreateConnectionDB(dbName)

	chkExists := fmt.Sprintf("select exists( SELECT schema_name FROM information_schema.schemata WHERE schema_name = '%s');", schemaName)
	rows, err := db.Query(chkExists)

	thd_log.CheckErr(err)

	exists := false
	for rows.Next() {

		err = rows.Scan(&exists)

		thd_log.CheckErr(err)

		break
	}
	defer rows.Close()

	// check errors
	thd_log.CheckErr(err)

	return exists
}

func CreateSchema(dbName string, schemaName string) bool {
	thd_log.PrintDebug("Schema <", schemaName, "> in Database <", dbName, "> will be created...")

	db, err := CreateConnectionDB(dbName)

	_, err = db.Exec("create schema " + schemaName + ";")

	thd_log.CheckErr(err)

	return true
}

func CheckTableExists(dbName string, schemaName string, tableName string) bool {
	thd_log.PrintDebug("Checking Table <", tableName, "> for Schema <", schemaName, "> in Database <", dbName, "> exists...")

	db, err := CreateConnectionDB(dbName)

	chkExists := fmt.Sprintf("select * from %s.%s LIMIT 1;", schemaName, tableName)
	_, err = db.Query(chkExists)

	//thd_log.CheckErr(err)

	exists := true
	if err != nil {
		exists = false;
	}

	return exists
}

func CreateTable(dbName string, schemaName string, tableName string, columnDef string) bool {
	thd_log.PrintDebug("Table <", tableName, "> for Schema <", schemaName, "> in Database <", dbName, "> will be created... \n Column definition <", columnDef,">")

	db, err := CreateConnectionDB(dbName)

	_, err = db.Exec("CREATE TABLE " + schemaName + "." + tableName + " ( " + columnDef + " );")

	thd_log.CheckErr(err)

	return true
}

func CheckUserExists(userName string) bool {
	thd_log.PrintDebug("Checking User <", userName, "> exists...")

	db, err := CreateConnectionDBAdmin()

	chkExists := fmt.Sprintf("select exists(SELECT 1 FROM pg_roles WHERE rolname='%s')", userName)
	rows, err := db.Query(chkExists)

	thd_log.CheckErr(err)

	exists := false
	for rows.Next() {

		err = rows.Scan(&exists)

		thd_log.CheckErr(err)

		break
	}
	defer rows.Close()

	// check errors
	thd_log.CheckErr(err)

	return exists
}

func CreateUser(dbName string, userName string, userPass string) bool {
	thd_log.PrintDebug("User <", userName, "> in Database <", dbName, "> will be created...")

	db, err := CreateConnectionDBAdmin()

	createStatement := fmt.Sprintf("CREATE USER %s WITH ENCRYPTED PASSWORD '%s'; GRANT ALL PRIVILEGES ON DATABASE %s TO %s;", userName, userPass, dbName, userName)
	_, err = db.Query(createStatement)

	// check errors
	thd_log.CheckErr(err)

	return true;
}

func SetupDB(dbName string) bool {
	thd_log.PrintInfo("Check DB <", dbName, "> ...")

	created := false

	if false == CheckDatabaseExists(dbName) {
		thd_log.PrintDebug("Create DB <", dbName, "> ...")
		created = CreateDatabase(dbName)
		if created {
			thd_log.PrintDebug("DB <", dbName, "> created")
		}
	} else {
		thd_log.PrintDebug("DB <", dbName, "> already exists.")
		created = true
	}

	thd_log.PrintInfo("Check DB finished.")

	return created
}

func SetupSchema(dbName string, schemaName string) bool {
	thd_log.PrintInfo("Check Schema <", schemaName, "> ...")

	created := false

	if false == CheckSchemaExists(dbName, schemaName) {
		thd_log.PrintDebug("Create Schema <", schemaName, "> ...")
		created = CreateSchema(dbName, schemaName)
		if created {
			thd_log.PrintDebug("Schema <", schemaName, "> created")
		}
	} else {
		thd_log.PrintDebug("Schema <", schemaName, "> already exists.")
		created = true
	}

	thd_log.PrintInfo("Check Schema finished.")

	return created
}

func SetupTable(dbName string, schemaName string, tableName string, columnDef string) bool {
	thd_log.PrintInfo("Check Table <", tableName, "> ...")

	created := false

	if false == CheckTableExists(dbName, schemaName, tableName) {
		thd_log.PrintDebug("Create Table <", tableName, "> ...")
		created = CreateTable(dbName, schemaName, tableName, columnDef)
		if created {
			thd_log.PrintDebug("Table <", tableName, "> created")
		}
	} else {
		thd_log.PrintDebug("Table <", tableName, "> already exists.")
		created = true
	}

	thd_log.PrintInfo("Check Schema finished.")

	return created
}

func UpsertInTable(dbName string, schemaName string, tableName string, columns string, values string, conflictColums string, updateStmt string) bool  {
	//INSERT INTO customers (name, email)
	//VALUES('Microsoft','hotline@microsoft.com')
	//ON CONFLICT (name)
	//DO
	//UPDATE SET email = EXCLUDED.email || ';' || customers.email;

	//INSERT INTO tablename (a, b, c) values (1, 2, 10)
	//ON CONFLICT (a) DO UPDATE SET c = tablename.c + 1;

	thd_log.PrintDebug("upsert in table <", tableName, "> of Database <", dbName, ">...")

	db, err := CreateConnectionDB(dbName)

	createStatement := fmt.Sprintf("INSERT INTO %s.%s (%s) values (%s) ON CONFLICT (%s) DO UPDATE SET %s;", schemaName, tableName, columns, values, conflictColums, updateStmt)
	_, err = db.Query(createStatement)

	// check errors
	thd_log.CheckErr(err)

	return true
}

func SelectFromTable(dbName string, schemaName string, tableName string, selectColumns string, condition string) []map[string]interface{} {
	thd_log.PrintDebug("select from table <", tableName, "> of Database <", dbName, ">...")

	db, err := CreateConnectionDB(dbName)

	if len(selectColumns) <= 0 {
		selectColumns = "*"
	}

	if selectColumns == "*" {
		thd_log.PrintError("Select by * not supported")
		return []map[string]interface{}{} // return empty map
	}

	selectStatement := fmt.Sprintf("SELECT %s FROM  %s.%s %s;", selectColumns, schemaName, tableName, condition)
	rows, err := db.Query(selectStatement)

	thd_log.CheckErr(err)

	columns := strings.Split(selectColumns, ",")
	columnCount := len(columns)
	// for each database row / record, a map with the column names and row values is added to the allMaps slice
	var allMaps []map[string]interface{}

	for rows.Next() {
		values := make([]interface{}, columnCount)
		pointers := make([]interface{}, columnCount)
		for i,_ := range values {
			pointers[i] = &values[i]
		}
		err := rows.Scan(pointers...)
		resultMap := make(map[string]interface{})
		for i,val := range values {
			resultMap[columns[i]] = val
		}
		allMaps = append(allMaps, resultMap)

		thd_log.CheckErr(err)
	}
	defer rows.Close()

	// check errors
	thd_log.CheckErr(err)

	return allMaps
}
