package main

import (
	"fmt"
	"goDbManager/thd_log"
	"goDbManager/thd_postgres"
)

const DbInternalManager string = "dbManager"
const DbInternalManagerSchema string = "public"

const dbPrefixDefault = "app_"
const dbSchemaDefault = "public"

func MaintainInternalDb() bool {
	thd_log.PrintInfo("Wait for remote DB <")

	dbCreated := thd_postgres.SetupDB(DbInternalManager)
	if false == dbCreated {
		thd_log.PrintError("Internal manager DB <", DbInternalManager, "> could not been created")
		return false
	}

	schemaCreated := thd_postgres.SetupSchema(DbInternalManager, DbInternalManagerSchema)
	if false == schemaCreated {
		thd_log.PrintError("Internal manager DB <", DbInternalManager, "> Schema <", DbInternalManagerSchema, "> could not been created")
		return false
	}

	tDefApplications := GetTableDefApplications()
	colDef := "id " + tDefApplications.Id
	colDef = colDef + ", DataId " + tDefApplications.DataId
	colDef = colDef + ", Data " + tDefApplications.Data
	success := thd_postgres.SetupTable(DbInternalManager, DbInternalManagerSchema, tDefApplications.Name, colDef)

	return success
}

func buildAppDbName (appId string) string {
	if len(appId) <= 0 {
		return ""
	}

	return dbPrefixDefault + appId
}

func MaintainAppDb(appId string) bool {
	if len(appId) <= 0 {
		return false
	}

	dbName := buildAppDbName(appId)

	dbCreated := thd_postgres.SetupDB(dbName)
	if false == dbCreated {
		thd_log.PrintError("App DB <", dbName, "> could not been created")
		return false
	}

	schemaCreated := thd_postgres.SetupSchema(dbName, dbSchemaDefault)
	if false == schemaCreated {
		thd_log.PrintError("App DB <", dbName, "> Schema <", dbSchemaDefault, "> could not been created")
		return false
	}

	return  true
}

func MaintainAppDbTable(appId string, tableName string) bool {
	if len(appId) <= 0 || len(tableName) <= 0{
		return false
	}

	dbName := buildAppDbName(appId)

	tDefDefault := GetTableDefAppDefaultTable(tableName)
	colDef := "id " + tDefDefault.Id
	colDef = colDef + ", DataId " + tDefDefault.DataId
	colDef = colDef + ", Data " + tDefDefault.Data
	success := thd_postgres.SetupTable(dbName, dbSchemaDefault, tableName, colDef)

	return success
}

func CheckExistsAppDbTable(appId string, tableName string) bool {
	if len(appId) <= 0 || len(tableName) <= 0{
		return false
	}

	dbName := buildAppDbName(appId)

	return thd_postgres.CheckTableExists(dbName, dbSchemaDefault, tableName)
}

func UpsertAppDbTable(appId string, tableName string, content AppDefaultTable) bool {
	if len(appId) <= 0 || len(tableName) <= 0{
		return false
	}

	dbName := buildAppDbName(appId)

	colums := "dataId, data"
	values := "'" + content.DataId + "', '" + content.Data + "'"
	conflictColums := "dataId"
	updateStmt := "data = '" + content.Data + "'"

	success := thd_postgres.UpsertInTable(dbName, dbSchemaDefault, tableName, colums, values, conflictColums, updateStmt)

	return success
}

func SelectFromTable(appId string, tableName string, content AppDefaultTable) []map[string]interface{} {
	if len(appId) <= 0 || len(tableName) <= 0{
		return nil
	}

	dbName := buildAppDbName(appId)

	whereClause := fmt.Sprintf("WHERE DataId like '%s'", content.DataId)

	rows := thd_postgres.SelectFromTable(dbName, dbSchemaDefault, tableName, "id, DataId, Data", whereClause)

	return  rows
}
