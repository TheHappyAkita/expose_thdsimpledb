## Preconditions
- docker
- docker-compose
- go lang for IDE

## Intention
This project was created to get in touch with the go language.
It consists the following features:

- restful database (postgres) insert / update / read
- docker: postgres db
- docker: using go lang directly
- docker: cross compiling inside docker for windows and linux
- docker: using compiled binaries

## Docker Usage
**postgres db**
```bash
docker-compose start db
```

**dev container - which contains full go lang**
```bash
docker-compose up --build godbm_dev
```

**compile container - which generates the binaries for linux / windows**
```bash
docker-compose up --build compile_godbm
```

**binary container - which only contains the binary for linux**
```bash
docker-compose up --build godbm
```

## HTTP Call Examples:
**Ping**
```bash
curl --request GET -sL --url http://localhost:50007/ping
```

**Create a DB called "testDb"**
```bash
curl -X POST -H "Content-type: application/json" -H "Accept: application/json" --url http://localhost:50007/app/testDb
```

**Check if a DB called "testDb" is available**
```bash
curl -X GET -H "Content-type: application/json" -H "Accept: application/json" --url http://localhost:50007/app/testDb
```

**Insert / Update (by Id in data) the given data into a table called "testTable" in DB "testDB" - creates DB and Table if it doesnt exist already**
```bash
curl -X POST -H "Content-type: application/json" -H "Accept: application/json" -d '{"Id":"myId", "Data": "{\"someTestData\": \"common content\"}"}' http://localhost:50007/app/testDb/table/testTable
```

**Select (by Id in data) the data from a table called "testTable" in DB "testDB"**
```bash
curl -X GET -H "Content-type: application/json" -H "Accept: application/json" -d '{"Id":"myId", "Data": ""}' http://localhost:50007/app/testDb/table/testTable
```
